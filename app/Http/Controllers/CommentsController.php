<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Blog;

class CommentsController extends Controller
{
    // public function __construct(){
    //     $this->middleware(['validateAdmin'])->only(['edit','update','destroy']);
    // }
    public function index(){
        $comments = Comment::latest()->paginate(5);
        return view('admin.comments.index',compact(['comments']));
    }

    public function verify(Comment $comment){
        // dd("there");
        if($comment->verified_at){
            $comment->verified_at = null;
            $comment-save();
            session()->flash('error','Comment Unverified');
            
        }else{
            $comment->verified_at = now();
            // dd("here");
            $comment->save();
            session()->flash('success','Comment verified');
        }

        
        return redirect(route('admin.comments.index'));
    }
      
    
}
