<?php

namespace App\Http\Controllers;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Requests\Blogs\CreateBlogRequest;
use App\Http\Requests\Blogs\UpdateBlogRequest;
use App\Http\Middleware\ValidateAuthor;

class BlogsController extends Controller
{
    public function __construct(){
        $this->middleware(['validateAuthor'])->only(['edit','update','destroy','trash']);
    }
    public function index()
    {
        $authUser = auth()->user();
        if($authUser->isAdmin()){
            $blogs = Blog::with('category')->latest()->paginate(10);
        }else{
            $blogs = Blog::with('category')
                          ->where('user_id',$authUser->id)
                          ->published()
                          ->latest()
                          ->paginate(10);
        }
        
        return view('admin.blogs.index', compact('blogs'));
    }

    public function create(){
        $categories = Category::all();
        $tags = Tag::all();

        return view('admin.blogs.create',compact(['categories','tags']));
    }

    public function store(CreateBlogRequest $request){
        // Image Upload and return the name of the file which will be created
        $image_path = $request->file('image')->store('blogs');
        $data = $request->only(['title','excerpt','body','category_id','published_at']);
        
        $data = array_merge($data, [
            'image_path'=>$image_path,
            'user_id'=>auth()->user()->id
        ]);


        $blog = Blog::create($data);
        // dd($request->tags);
        $blog->tags()->attach($request->tags);

        session()->flash('success','Blog created successfully...');
        return redirect(route('admin.blogs.index'));

    }

    public function edit(Blog $blog){
        $categories = Category::all();
        $tags = Tag::all();

        return view('admin.blogs.edit',compact([
            'categories',
            'tags',
            'blog'
        ]));
    }

    public function update(UpdateBlogRequest $request, Blog $blog) {
        $data = $request->only(['title','excerpt','body','category_id','published_at']);

        if($request->hasFile('image')){
            $image_path = $request->file('image')->store('blogs');
            $blog->deleteImage();
            $data = array_merge($data, ['image_path'=>$image_path]);

        }
        $blog->update($data);
        $blog->tags()->sync($request->tags);

        session()->flash('success','Blog Updated successfully...');
        return redirect(route('admin.blogs.index'));
    }

    public function trash(Blog $blog){
        $blog->delete();

        session()->flash('success','Blog Deleted successfully...');
        return redirect(route('admin.blogs.index'));
    }

    public function destroy(int $blogId){
        $blog = Blog::onlyTrashed()->find($blogId);
        $blog->deleteImage();
        $blog->forceDelete();

        session()->flash('success','Blog Destroyed successfully...');
        return redirect(route('admin.blogs.trashed'));
    }

    public function trashed(){
        $blogs = Blog::with('category')
                ->where('user_id',auth()->id())
                ->onlyTrashed()
                ->latest()
                ->paginate(10);
            
        return view('admin.blogs.trashed', compact('blogs'));
    }

    public function restore(int $blogId){
        $blog = Blog::withTrashed()->find($blogId);
        $blog->restore();

        session()->flash('success','Blog Restored successfully...');
        return redirect(route('admin.blogs.index'));
    }

    public function verify(){
        
        $blogs = Blog::with('category')->latest()->paginate(10);
        return view('admin.blogs.verify',compact(['blogs']));
    }



    public function verifyBlog(Blog $blog){
        $blog->blog_status = "verified";
        $blog->save();
        
        session()->flash('success','Blog Restored successfully...');
        return redirect(route('admin.blogs.verifyblogs'));
       
    }

    public function unverifyBlog(Blog $blog){
        $blog->blog_status = "unverify";
        $blog->save();
        session()->flash('success','Blog Removed from Frontend panel...');
        return redirect(route('admin.blogs.verifyblogs'));

    }

    public function draft(){
        $blogs = Blog::with('category')
                ->where('user_id',auth()->id())
                ->whereNull('published_at')
                ->latest()
                ->paginate(5);       
        return view('admin.blogs.draft',compact(['blogs']));
    }

    public function restoreBlog(Blog $blog){
        $blog->published_at = now();
        $blog->save();

        session()->flash('success','Blog Restored!');
        return redirect(route('admin.blogs.index'));

    }

    public function drafted(Blog $blog){
        $blog->published_at = null;
        $blog->save();
        // ss($blog);
        session()->flash('success','Blog Drafted!');
        return redirect(route('admin.blogs.index'));
    }

    
    

}
