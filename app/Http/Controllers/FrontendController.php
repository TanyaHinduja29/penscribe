<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Comments\CreateCommentRequest;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Tag;
use App\Models\User;
use App\Models\Comment;


class FrontendController extends Controller
{
    public function index(){
        $blogs = Blog::with('author')
                ->search()
                ->published()
                ->verified()
                ->orderBy('published_at','desc')
                ->simplePaginate(3);
        
        $categories   = Category::withCount('blogs')->get();
        $tags = Tag::limit(10)->get();
        return view('frontend.index',compact([
            'categories',
            'tags',
            'blogs'
        ]));
    }

    public function category(Category $category){
        $blogs = $category->blogs()->search()->published()->simplePaginate(3);
        $categories = Category::withCount('blogs')->get();
        $tags = Tag::limit(10)->get();
        return view('frontend.index',compact([
            'categories',
            'tags',
            'blogs'
        ]));
    }

    public function tag(Tag $tag){
        $blogs = $tag->blogs()->search()->published()->simplePaginate(3);
        $categories = Category::withCount('blogs')->get();
        $tags = Tag::limit(10)->get();
        return view('frontend.index',compact([
            'categories',
            'tags',
            'blogs'
        ]));

    }

    public function show(Blog $blog){
        $blogTags = $blog->tags;
        $categories = Category::withCount('blogs')->get(); #for sidebar
        $tags = Tag::limit(10)->get(); #for sidebar
        $comments = Comment::where('blog_id',$blog->id)->get();
        return view('frontend.blog',compact([
            'categories',
            'tags',
            'blog',
            'blogTags',
            'comments'
        ]));
    }
    
    public function comment(CreateCommentRequest $request,Blog $blog){
        // dd("reaching till here");
        // dd($request);
        $comment = $request->comment;
        $blog_id = $blog->id;

        // $data = array_merge($comment, [
        //     'user_id'=>$user_id,
        //     'blog_id'=>$blog_id]);
        // $comment_obj = Comment::create($data)
        if(auth()->user()){
            
           $user=auth()->user();
            if($user->isVerified()){
                Comment::create([
                    
                    'blog_id'=>$blog_id,
                    'user_name'=>auth()->user()->name,
                    'email'=>auth()->user()->email,
                    'comment'=>$comment,
                    'role'=>'author'
                
                ]);
                session()->flash('success','sucess');
            }else{
                session()->flash('success','verify your email first');
            }
           
        }else{
            Comment::create([
                
                'blog_id'=>$blog_id,
                'user_name'=>$request->name,
                'email'=>$request->email,
                'comment'=>$comment,
                'role'=>'reader'
            ]);
            session()->flash('success','login first');
        }
        return redirect(route('frontend.blogs.show',$blog_id));
        
        // dd($comment);
    }
    public function delete(Comment $comment){
        // dd($comment);
        // $comment = Comment::find($comment_id);
        $comment->forceDelete();
        // $comment->save();
        // $blog_id = $blog->id;
        // dd($blog_id);
        return redirect(route('frontend.blogs.show',$comment->blog_id));
    } 
}
