<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\ProfileUpdateRequest;

class UsersController extends Controller
{
    public function index(){
        $users = User::paginate(5);
        return view('admin.Users.user',compact(['users']));
    }

    public function verify(User $user){
        // dd("hello im verified");
        // $email_verified_at = [now()];
        // // dd($email_verified_at);
        // $user->update($email_verified_at)
        //       ->where('user_id',$user->id);
        
        $user->email_verified_at = now();
        $user->save();
        session()->flash('success','Email Verified successfully...');
        return redirect(route('admin.users.index'));
        // dd($user);

    }
    public function unverify(User $user){
        $user->email_verified_at = NULL;
        if($user->role == 'admin'){
            $user->role = 'author';
        }
        $user->save();
        session()->flash('success','Email Unverified successfully...');
        return redirect(route('admin.users.index'));
        // dd("hello i am unverified");
    }

    public function makeAdmin(User $user){
        if($user->email_verified_at){
            $user->role="admin";
            $user->save();
            session()->flash('success','Congrats! You are an admin now.');
            // dd("Yay you are eleigible");
        }else{
            session()->flash('error','Cannot be switched to Admin. Please verify email first!');
        }
        
        return redirect(route('admin.users.index'));
    }

    public function makeAuthor(User $user){
        $user->role="author";
        $user->save();
        session()->flash('success','Role Switched to Author only...');
        return redirect(route('admin.users.index'));
    }
}
