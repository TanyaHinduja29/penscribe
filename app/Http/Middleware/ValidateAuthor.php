<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Blog;
use Symfony\Component\HttpFoundation\Response;

class ValidateAuthor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        // dd($request->blog);
        if(!is_Object($request->blog)){
            $blog = Blog::onlyTrashed()->find($request->blog);
            // dd($blog);
        }else{
            $blog = $request->blog;
            // dd($blog);
        }
        // dd($blog);
        if(!auth()->user()->isOwner($blog)){
            return abort(401);
        }
        return $next($request);
    }
}
