<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\TagsController;
use App\Http\Controllers\BlogsController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\CommentsController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/',[FrontendController::class,'index'])->name('home');
Route::get('/categories/{category}/blogs',[FrontendController::class,'category'])->name('frontend.category');
Route::get('/tags/{tag}/blogs',[FrontendController::class,'tag'])->name('frontend.tag');
Route::get('/blogs/{blog}',[FrontendController::class,'show'])->name('frontend.blogs.show');
Route::post('/blogs/{blog}/comment',[FrontendController::class,'comment'])->name('frontend.blogs.comment');
Route::delete('/comments/{comment}/delete',[FrontendController::class,'delete'])->name('frontend.comments.delete');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {

    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::prefix('/admin')->name('admin.')->group(function () {
        Route::get('/dashboard', function () {
            return view('admin.dashboard');
        })->name('dashboard');

        Route::get('/blogs/trashed',[BlogsController::class,'trashed'])->name('blogs.trashed');
        Route::get('/blogs/verify',[BlogsController::class,'verify'])->name('blogs.verifyblogs');
        Route::delete('/blogs/{blog}/trash',[BlogsController::class,'trash'])->name('blogs.trash');
        Route::put('/blogs/{blog}/restore',[BlogsController::class,'restore'])->name('blogs.restore');
        Route::post('/blogs/{blog}/verify',[BlogsController::class,'verifyBlog'])->name('blogs.verify');
        Route::post('/blogs/{blog}/unverify',[BlogsController::class,'unverifyBlog'])->name('blogs.unverify');

        Route::get('/blogs/draft',[BlogsController::class,'draft'])->name('blogs.draft');
        Route::post('/blogs/{blog}/restore',[BlogsController::class,'restoreBlog'])->name('blogs.restoreblog');
        Route::post('/blogs/{blog}/drafted',[BlogsController::class,'drafted'])->name('blogs.drafted');
        
        Route::post('/users/{user}/verify',[UsersController::class,'verify'])->name('users.verify');
        Route::post('/users/{user}/unverify',[UsersController::class,'unverify'])->name('users.unverify');
        Route::post('/users/{user}/makeAdmin',[UsersController::class,'makeAdmin'])->name('users.makeAdmin');
        Route::post('/users/{user}/makeAuthor',[UsersController::class,'makeAuthor'])->name('users.makeAuthor');
        
        Route::post('/comments/{comment}/verify',[CommentsController::class,'verify'])->name('comments.verify');
        

        Route::resource('categories', CategoriesController::class)->except(['show']);
        Route::resource('tags', TagsController::class);
        Route::resource('blogs', BlogsController::class);
        Route::resource('users',UsersController::class);  
        Route::resource('comments',CommentsController::class);      
    });
});

require __DIR__.'/auth.php';
