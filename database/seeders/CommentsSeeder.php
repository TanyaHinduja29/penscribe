<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Blog;
use App\Models\Comment;
class CommentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
       
        $blogs = Blog::all();
        $comment1 = Comment::create([
            'comment'=>fake()->sentence(6),
            
            'blog_id'=>$blogs->random()->id
        ]);
        $comment2 = Comment::create([
            'comment'=>fake()->sentence(6),
            
            'blog_id'=>$blogs->random()->id
        ]);
        $comment3 = Comment::create([
            'comment'=>fake()->sentence(6),
            
            'blog_id'=>$blogs->random()->id
        ]);
        $comment4 = Comment::create([
            'comment'=>fake()->sentence(6),
            
            'blog_id'=>$blogs->random()->id
        ]);
    }
}
