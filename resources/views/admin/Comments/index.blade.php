@extends('admin.layout.app')

@section('main-content')
    <div class="container-fluid">
        <!--Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        </div>
        @include('admin.layout._alert-messages')
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Verified_by</th>
                        <th>Blog Id</th>
                        <th>Actions</th>
                    </thead>
                    <tbody>
                        @foreach ($comments as $comment)
                            <tr>
                                <td>{{ $comment->id }}</td>
                                <td>{{ $comment->comment }}</td>
                                <td>{{$comment->verified_by}}</td>
                                <td>{{$comment->blog_id}}</td>
                                <td>
                                    @if($comment->verified_at)
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#unverifyModal"
                                            onclick="unverifyModalHelper('{{ route('admin.comments.verify', $comment->id) }}')">
                                            Unverify Comment
                                        </button>
                                    @else
                                        <button class="btn btn-danger" data-toggle="modal" data-target="#verifyModal"
                                            onclick="verifyModalHelper('{{ route('admin.comments.verify', $comment->id) }}')">
                                            Verify Comment
                                        </button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $comments->links('vendor.pagination.bootstrap-5') }}
            </div>
        </div>
    </div>
    {{-- Verify Modal --}}
    <div class="modal fade" id="verifyModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="" method="POST" id="verifyForm">
                @csrf
                @method('POST')
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Verify Comment?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">If you verify the comment will be shown publicly.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-secondary" type="submit">Verify</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

    {{-- Unverify Modal --}}
    <div class="modal fade" id="unverifyModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="" method="POST" id="unverifyForm">
                @csrf
                @method('POST')
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Unverify Comment?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">If you unverify the comment will be hidden from the frontend panel.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-secondary" type="submit">Unverify</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function verifyModalHelper(url) {
            //console.log(url); //http://localhost:8000/admin/categories/2
            document.getElementById("verifyForm").setAttribute('action', url);
        }
        function unverifyModalHelper(url) {
            //console.log(url); //http://localhost:8000/admin/categories/2
            document.getElementById("unverifyForm").setAttribute('action', url);
        }
    </script>
@endsection
