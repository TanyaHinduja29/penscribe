@extends('admin.layout.app')

@section('main-content')
    <div class="container-fluid">
        <!--Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            
        </div>
        @include('admin.layout._alert-messages')
        @method('POST')
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Email verified</th>
                        <th>Role</th>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    @method('POST')
                                    @if($user->email_verified_at)
                                        <button class="btn btn-primary" data-toggle="modal"              data-target="#unverifyModal"
                                                    onclick="unverifyModalHelper('{{ route('admin.users.unverify', $user->id) }}')"> Unverify Email
                                                    
                                        </button>
                                    @else
                                        <button class="btn btn-primary" data-toggle="modal"               data-target="#verifyModal"
                                                onclick="verifyModalHelper('{{ route('admin.users.verify', $user->id) }}')"> Verify Email
                                                
                                        </button>
                                        
                                    @endif
                                </td>
                                <td>{{$user->role}}</td>
                                <td>
                                    
                                    @if($user->role =='author')
                                        <button class="btn btn-primary" data-toggle="modal"               data-target="#adminModal"
                                                    onclick="adminModalHelper('{{ route('admin.users.makeAdmin', $user->id) }}')"> Make as Admin    
                                        </button>
                                    @else
                                    <button class="btn btn-primary" data-toggle="modal"               data-target="#authorModal"
                                                    onclick="authorModalHelper('{{ route('admin.users.makeAuthor', $user->id) }}')"> Make as Author   
                                    </button>
                                    @endif
                                </td>
                                
                                <td>
                                    <a href="{{ route('admin.tags.edit', $user->id) }}" class="btn btn-primary">
                                        <i class="fas fa-pen"></i>
                                    </a>
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal"
                                        onclick="deleteModalHelper('{{ route('admin.tags.destroy', $user->id) }}')">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $users->links('vendor.pagination.bootstrap-5') }}
            </div>
        </div>
    </div>
    {{-- Delete Modal --}}
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="" method="POST" id="deleteForm">
                @csrf
                @method('DELETE')
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete category?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Are you sure you want to delete the category?</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-secondary" type="submit">Delete</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

    {{-- Verify Modal --}}
    <div class="modal fade" id="verifyModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="" method="POST" id="verifyForm">
                @csrf
                @method('POST')
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Verify Email?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Are you sure you want to verify this user's email?</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-secondary" type="submit">Verify</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

    {{-- UnVerify Modal --}}
    <div class="modal fade" id="unverifyModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="" method="POST" id="unverifyForm">
                @csrf
                @method('POST')
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Unverify Email?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Are you sure you want to unverify this user's email?</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-secondary" type="submit">UnVerify</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

    
    {{-- Admin Modal --}}
    <div class="modal fade" id="adminModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="" method="POST" id="adminForm">
                @csrf
                @method('POST')
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Make Admin?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Are you sure you want to Switch the user role to Admin?</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-secondary" type="submit">Switch</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

    
    {{-- Author Modal --}}
    <div class="modal fade" id="authorModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="" method="POST" id="authorForm">
                @csrf
                @method('POST')
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Make Author?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Do you want to switch the user role to Author?</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-secondary" type="submit">Switch</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function deleteModalHelper(url) {
            console.log(url);
            document.getElementById("deleteForm").setAttribute('action', url);
        }
        function verifyModalHelper(url) {
            console.log(url);
            document.getElementById("verifyForm").setAttribute('action', url);
        }
        function unverifyModalHelper(url) {
            console.log(url);
            document.getElementById("unverifyForm").setAttribute('action', url);
        }
        function adminModalHelper(url) {
            console.log(url);
            document.getElementById("adminForm").setAttribute('action', url);
        }
        function authorModalHelper(url) {
            console.log(url);
            document.getElementById("authorForm").setAttribute('action', url);
        }
    </script>
@endsection
